package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public class MyGdxGame extends ApplicationAdapter {

    ShapeRenderer shapeRenderer;
    SpriteBatch batch;
    BitmapFont font;

    @Override
    public void create() {
        shapeRenderer = new ShapeRenderer();
        batch = new SpriteBatch();
        font = new BitmapFont();
    }

    private float frand( float max ) {
        return max * (float)Math.random();
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor( 1, 0, 0, 1 );
        Gdx.gl.glClear( GL20.GL_COLOR_BUFFER_BIT );
        float baseX = frand( 400 );
        float baseY = frand( 400 );
        for( int i = 0; i < 100; i++ ) {
            FilledPolygon.render( new float[]{
                            frand( baseX + 10 ),
                            frand( baseY + 10 ),
                            frand( baseX + 100 ),
                            frand( baseY + 50 ),
                            frand( baseX + 10 ),
                            baseY + 100 + frand( 10 ) },
                    frand( 1f ), frand( 1f ), frand( 1f ), 0.5f + frand( 0.5f ) );
        }
        batch.begin();
        font.draw(batch, "fps: " + Gdx.graphics.getFramesPerSecond(), 20, 30);
        batch.end();
    }
}
