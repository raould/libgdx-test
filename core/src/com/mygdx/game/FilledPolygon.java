package com.mygdx.game;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.*;
import com.badlogic.gdx.utils.*;

/**
 * Created by superman on 9/25/14.
 */
public class FilledPolygon {
    private static final EarClippingTriangulator s_triangulator = new EarClippingTriangulator();
    private static final ShapeRenderer s_shapeRenderer = new ShapeRenderer(50); // todo: arbitrary.

    public static void render( final float[] vertices, final float r, final float g, final float b, final float a ) {
        s_shapeRenderer.begin( ShapeRenderer.ShapeType.Filled );
        s_shapeRenderer.setColor( r, g, b, a );
        final ShortArray tvs = s_triangulator.computeTriangles( vertices );
        for( int i = 0; i < tvs.size; i += 3 ) {
            final short v0 = tvs.get(i*3);
            final short v1 = tvs.get(i*3+1);
            final short v2 = tvs.get(i*3+2);
            s_shapeRenderer.triangle( vertices[v0], vertices[v0+1],
                                      vertices[v1], vertices[v1+1],
                                      vertices[v2], vertices[v2+1] );
        }
        s_shapeRenderer.end();
    }
}
